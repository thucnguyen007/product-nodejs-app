# Application

## Dev

1. CI/CD
[![pipeline status](https://gitlab.com/thucnguyen007/product-nodejs-app/badges/main/pipeline.svg)](https://gitlab.com/thucnguyen007/product-nodejs-app/-/commits/main)

2. Create a file named `.env` and fill in the following variables:

```
PORT=8000

URI_MONGODB=mongodb+srv://product-api:y9A5BKSmnG4qzXy@firstcluster.gwfxw.mongodb.net/?retryWrites=true&w=majority
SECRET=ninjadojoshifuyoshimarioluigipeachbowser

REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=redispass
```