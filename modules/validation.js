const Joi = require('joi');

const userValidate = data => {
    const userSchema1 = Joi.object({
        email: Joi.string().email().lowercase().required(),
        password: Joi.string().min(2).max(32).required(),
    });

    return userSchema1.validate(data);
}

module.exports = {
    userValidate
}