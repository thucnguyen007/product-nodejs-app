echo 'Creating application user and db'

mongo ${DB_NAME} \
  --host db \
  --port 12707 \
  -u mongo_user \
  -p mongo_pass \
  --authenticationDatabase admin \
  --eval "db.createUser({user: '${myuser}}', pwd: '${myuserpass}', roles:[{role:'dbOwner', db: '${my_db}'}]});"
